package com.phonecompany.billing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private BigDecimal result;
    private Calculator calculator;
    private String phoneLog = """
            420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57
            420776562353,18-01-2020 08:59:20,18-01-2020 09:10:00
            420776562453,18-01-2020 08:59:20,18-01-2020 09:04:00
            420774577453,13-01-2020 08:59:20,13-01-2020 09:15:00
            420776562353,18-01-2020 08:59:20,18-01-2020 09:18:00
            """;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void calculate() {
        result = calculator.calculate(phoneLog);
        assertEquals(14.5,result.doubleValue());
    }
}