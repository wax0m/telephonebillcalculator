package com.phonecompany.billing;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class Calculator implements TelephoneBillCalculator {
    class Log {
        private final String phoneNumber;
        private double cost;
        private int count;

        public double getCost() {
            return cost;
        }

        public int getCount() {
            return count;
        }

        Log (String phoneNumber) {
            this.phoneNumber = phoneNumber;
            this.cost = 0;
            this.count = 1;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Log log = (Log) o;
            return Objects.equals(phoneNumber, log.phoneNumber);
        }

        @Override
        public String toString() {
            return "%s: %d times called, cost %.2f".formatted(phoneNumber, count, cost);
        }
    }

    private Scanner scanner;
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private List<Log> phoneNumbers = new ArrayList<>();
    private LocalDateTime startDT;
    private LocalDateTime endDT;
    private LocalTime normalStart;
    private LocalTime normalEnd;
    private String[] data;
    private BigDecimal totalCost;

    @Override
    public BigDecimal calculate(String phoneLog) {
        double costPerCall;
        scanner = new Scanner(phoneLog);
        normalStart = LocalDateTime.parse("16-11-2023 08:00:00", dateFormatter).toLocalTime();
        normalEnd = LocalDateTime.parse("16-11-2023 16:00:00", dateFormatter).toLocalTime();
        // Gets all the data from the input
        while (scanner.hasNext()) {
            data = scanner.nextLine().split(",");
            try {
                startDT = LocalDateTime.parse(data[1], dateFormatter);
                endDT = LocalDateTime.parse(data[2], dateFormatter);
            } catch (
                    DateTimeParseException e) {
                e.printStackTrace();
            }
            costPerCall = calculateCost(startDT, endDT);
            updateBill(data[0], costPerCall);
        }
        // Sort the log
        phoneNumbers.sort(Comparator.comparing(Log::getCount).thenComparing(Log::getCost).reversed());
        // Sets 0 to the most called number
        phoneNumbers.get(0).cost = 0.0f;
        totalCost = new BigDecimal(calculateTotalCost(), MathContext.DECIMAL64);
        totalCost = totalCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//        phoneNumbers.forEach(System.out::println);
        return totalCost;
    }

    private void updateBill(String phoneNumber, double cost) {
        Log log = new Log(phoneNumber);
        // Checks if phone number is already logged -> if not -> create a log
        if (!phoneNumbers.contains(log)) {
            phoneNumbers.add(log);
            log.cost += cost;
        } else {    // If exists -> update the log
            Log tmp = phoneNumbers.get(phoneNumbers.indexOf(log));
            tmp.count++;
            tmp.cost += cost;
        }
    }

    private double calculateCost(LocalDateTime start, LocalDateTime end) {
        double cost = 0f;
        LocalTime tmpStart = start.toLocalTime();
        LocalTime tmpEnd = end.toLocalTime();
        double duration = Duration.between(start, end).toSeconds() / 60f;
        if (duration > 5.0f) {
            cost = 5 + (duration-5) * 0.2f;                     // Call duration was over 5 mins
        }
        else if ((start.getHour() < normalEnd.getHour()         // Call duration was not over 5 mins and was in the interval
                    &&
                start.getHour() >= normalStart.getHour())
                    ||
                (end.getHour() >= normalStart.getHour()
                    &&
                end.getHour() < normalEnd.getHour())) {
            // Count for reduced price
            if (tmpStart.getHour() < normalStart.getHour()) {
                duration = Duration.between(tmpStart, normalStart).toSeconds();
                cost += duration * 0.5f;
                tmpStart = normalStart;
            }
            if (tmpEnd.getHour() >= normalEnd.getHour()) {
                duration = Duration.between(normalEnd, tmpEnd).toSeconds() / 60f;
                cost += duration * 0.5f;
                tmpEnd = normalEnd;
            }
            duration = Duration.between(tmpStart, tmpEnd).toSeconds() / 60f;
            cost += duration * 1;
        }
        else cost = duration * 1f;
        return cost;
    }

    private double calculateTotalCost() {
        double totalCost = 0f;
        for(int i = 0; i < phoneNumbers.size(); i++) {
            totalCost += phoneNumbers.get(i).cost;
        }
        return totalCost;
    }
}
